int photoresistor = 1;
int redCrosswalkHorizontal = 2;
int greenCrosswalkHorizontal = 3;
int redCarLightHorizontal = 4;
int yellowCarLightHorizontal = 5;
int greenCarLightHorizontal = 6;
int redCarLightRow = 7;
int yellowCarLightRow = 8;
int greenCarLightRow = 9;
int greenCrosswalkRow = 10;
int redCrosswalkRow = 11;
int buttonCrosswalkHorizontal = 12;
int buttonCrosswalkRow = 13;
bool night = false;
bool changedLight = false;
bool button1Pressed = false;
bool button2Pressed = false;

const uint16_t TRAFFIC_FLOW_TIME = 10000;
const uint16_t TRAFFIC_START_DELAY = 5000;
const uint16_t TRAFFIC_SWITCH_DELAY = 2000;

enum Trafficflow {row, horizontal};
Trafficflow flow;

void setup() {
  // put your setup code here, to run once:
  pinMode(photoresistor, INPUT);
  pinMode(redCrosswalkHorizontal, OUTPUT);
  pinMode(greenCrosswalkHorizontal, OUTPUT);
  pinMode(redCarLightHorizontal, OUTPUT);
  pinMode(yellowCarLightHorizontal, OUTPUT);
  pinMode(greenCarLightHorizontal, OUTPUT);
  pinMode(redCarLightRow, OUTPUT);
  pinMode(yellowCarLightRow, OUTPUT);
  pinMode(greenCarLightRow, OUTPUT);
  pinMode(greenCrosswalkRow, OUTPUT);
  pinMode(redCrosswalkRow, OUTPUT);
  pinMode(buttonCrosswalkHorizontal, INPUT_PULLUP);
  pinMode(buttonCrosswalkRow, INPUT_PULLUP);
  Serial.begin(115200);
  digitalWrite(redCrosswalkHorizontal, HIGH);
  digitalWrite(greenCarLightHorizontal, HIGH);
  digitalWrite(redCarLightRow, HIGH);
  digitalWrite(greenCrosswalkRow, HIGH);
  flow = horizontal;
}

void loop() {
  // put your main code here, to run repeatedly:
  uint16_t timestamp = millis();
  static uint16_t horizontal_crosswalk_timestamp;
  static uint16_t row_crosswalk_timestamp;
  static uint16_t traffic_flow_side_time;
  static uint16_t traffic_flow_horizontal_time;
  if(digitalRead(photoresistor) == LOW){
    button1Pressed = false;
    button2Pressed = false;
    night = false;
    Serial.println("It is day");
    digitalWrite(yellowCarLightHorizontal, LOW);
    digitalWrite(yellowCarLightRow, LOW);
    if(flow == horizontal){
      digitalWrite(redCrosswalkHorizontal, HIGH);
      digitalWrite(redCrosswalkRow, LOW);
      digitalWrite(greenCarLightHorizontal, HIGH);
      digitalWrite(redCarLightRow, HIGH);
      digitalWrite(greenCrosswalkRow, HIGH);
      digitalWrite(greenCrosswalkHorizontal, LOW);
    }
    else{
      digitalWrite(redCrosswalkRow, HIGH);
      digitalWrite(redCrosswalkHorizontal, LOW);
      digitalWrite(greenCarLightRow, HIGH);
      digitalWrite(redCarLightHorizontal, HIGH);
      digitalWrite(greenCrosswalkHorizontal, HIGH);
      digitalWrite(greenCrosswalkRow, LOW);
    }
    horizontal_crosswalk_timestamp = 0;
    row_crosswalk_timestamp = 0;
  }
  else{
    night = true;
    Serial.println("It is night");
    button1Pressed = false;
    button2Pressed = false;
    digitalWrite(greenCarLightHorizontal, LOW);
    digitalWrite(greenCarLightRow, LOW);
    digitalWrite(redCarLightHorizontal, LOW);
    digitalWrite(redCarLightRow, LOW);
    traffic_flow_side_time = 0;
    traffic_flow_horizontal_time = 0;
  }

  if(night){
    digitalWrite(greenCarLightHorizontal, LOW);
    digitalWrite(greenCarLightRow, LOW);
    digitalWrite(redCarLightHorizontal, LOW);
    digitalWrite(redCarLightRow, LOW);
    BlinkYellow();
    if (digitalRead(buttonCrosswalkHorizontal) == LOW && !button1Pressed){
      digitalWrite(redCrosswalkHorizontal, LOW);
      digitalWrite(greenCrosswalkHorizontal, HIGH);
      horizontal_crosswalk_timestamp = timestamp;
      button1Pressed = true;
      Serial.println("horizontal crosswalk open");
    }
    if(timestamp - horizontal_crosswalk_timestamp >= TRAFFIC_FLOW_TIME){
      digitalWrite(redCrosswalkHorizontal, HIGH);
      digitalWrite(greenCrosswalkHorizontal, LOW);
      button1Pressed = false;
      Serial.println("horizontal crosswalk closed");
    }
    if (digitalRead(buttonCrosswalkRow) == LOW && !button2Pressed){
      digitalWrite(redCrosswalkRow, LOW);
      digitalWrite(greenCrosswalkRow, HIGH);
      row_crosswalk_timestamp = timestamp;
      button2Pressed = true;
      Serial.println("row crosswalk open");
    }
    if(timestamp - row_crosswalk_timestamp >= TRAFFIC_FLOW_TIME){
      digitalWrite(redCrosswalkRow, HIGH);
      digitalWrite(greenCrosswalkRow, LOW);
      button2Pressed = false;
      Serial.println("row crosswalk closed");
    }
  }
  else{
    if (digitalRead(buttonCrosswalkHorizontal) == LOW && !button1Pressed && flow == horizontal){
      button1Pressed = true;
      Serial.println("Pressed horizontal button");
    }
    if(timestamp - traffic_flow_horizontal_time >= TRAFFIC_FLOW_TIME && button1Pressed){
      stopHorizontalTraffic();
      delay(TRAFFIC_START_DELAY);
      startSideTraffic();
      traffic_flow_side_time = timestamp;
      button1Pressed = false;
      flow = row;
      Serial.println("Traffic is flowing through the row");
    }
    if (digitalRead(buttonCrosswalkRow) == LOW && !button2Pressed && flow == row){
      button2Pressed = true;
      Serial.println("Pressed row button");
    }
    if(timestamp - traffic_flow_side_time >= TRAFFIC_FLOW_TIME && button2Pressed){
      stopSideTraffic();
      delay(TRAFFIC_START_DELAY);
      startHorizontalTraffic();
      traffic_flow_horizontal_time = timestamp;
      button2Pressed = false;
      flow = horizontal;
      Serial.println("Traffic is flowing through the column");
    }
  }
}
void BlinkYellow(){
  digitalWrite(yellowCarLightHorizontal, (millis() / 1000) % 2);
  digitalWrite(yellowCarLightRow, (millis() / 1000) % 2);
}
void startHorizontalTraffic(){
  digitalWrite(yellowCarLightHorizontal, HIGH);
  delay(TRAFFIC_SWITCH_DELAY);
  digitalWrite(redCrosswalkRow, LOW);
  digitalWrite(greenCrosswalkRow, HIGH);
  digitalWrite(redCarLightHorizontal, LOW);
  digitalWrite(yellowCarLightHorizontal, LOW);
  digitalWrite(greenCarLightHorizontal, HIGH);
}

void stopHorizontalTraffic(){
  digitalWrite(yellowCarLightHorizontal, HIGH);
  digitalWrite(greenCarLightHorizontal, LOW);
  digitalWrite(redCrosswalkRow, HIGH);
  digitalWrite(greenCrosswalkRow, LOW);
  delay(TRAFFIC_SWITCH_DELAY);
  digitalWrite(redCarLightHorizontal, HIGH);
  digitalWrite(yellowCarLightHorizontal, LOW);
}

void startSideTraffic(){
  digitalWrite(yellowCarLightRow, HIGH);
  delay(TRAFFIC_SWITCH_DELAY);
  digitalWrite(redCrosswalkHorizontal, LOW);
  digitalWrite(greenCrosswalkHorizontal, HIGH);
  digitalWrite(redCarLightRow, LOW);
  digitalWrite(yellowCarLightRow, LOW);
  digitalWrite(greenCarLightRow, HIGH);
}

void stopSideTraffic(){
  digitalWrite(redCrosswalkHorizontal, HIGH);
  digitalWrite(greenCrosswalkHorizontal, LOW);
  digitalWrite(yellowCarLightRow, HIGH);
  digitalWrite(greenCarLightRow, LOW);
  delay(TRAFFIC_SWITCH_DELAY);
  digitalWrite(redCarLightRow, HIGH);
  digitalWrite(yellowCarLightRow, LOW);
}